# Hawk Qt Controller
This is the main repository for Hawk Qt Controller.
A Python Program which helps you control your hawk module.
For further details about hawk please refer to this repository:

https://gitlab.com/steampunk_software1577/hawk-vision

and to Hawk GTK Controller repository:

https://gitlab.com/steampunk_software1577/hawk-gtk-controller

# Note
I highly recommends the GTK version over the Qt version for linux and unix users.

# What does it do?
Using this program you can control and configure your hawk modules.
In a nutshell hawk is a collection of scripts which enables FRC teams to develop image processing algorithms very easily.
Using Hawk controller you can:

-Control IP address 

-Change password for your processor computer

-Calibrate cameras settings

-Choose grip files and scripts

-Debug using the console

-Manage config files, scripts and grip files

# Basic usage
This program is a Qt "clone" of the GTK version and acts almost the same, so please refer to the instructions in Hawk GTK Controller repository.

# Dependencies
Python3

PyQt5 PyQt5-tools PyQtWebEngine

paramiko

# Note
The project is very early stages
Any help is welcomed

Also this project is a part of the hawk-vision project
https://gitlab.com/steampunk_software1577/hawk-vision

# Contact
For any request or question you can either submit an issue or email at:

oren_daniel@protonmail.com --original author

steampunk.software@gmail.com
